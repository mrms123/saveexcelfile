package acgPortal.saveExcelFile;

import static org.junit.Assert.assertTrue;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

public class ExcelUploaderTest {

	@Test
	public void testInsertData() throws Exception {
		String filePath = "C:\\Users\\priyanka.tuteja\\Desktop\\OpRisk_TimeSheet.xlsx";
		ExcelUploader test = new ExcelUploader(filePath);
		int rowOffset = 7;
		test.setRowOffset(rowOffset);
		test.insertData();
	}

	@Test
	public void checkfileFormatTest() {
		String filePath = "C:\\Users\\priyanka.tuteja\\Desktop\\OpRisk_TimeSheet.xlsx";
		ExcelUploader test = new ExcelUploader(filePath);
		assertTrue("Fail", test.checkfileFormat());
	}

	@Test
	public void testDisplayData() throws Exception {
		String filePath = "C:\\Users\\priyanka.tuteja\\Desktop\\OpRisk_TimeSheet.xlsx";
		ExcelUploader test = new ExcelUploader(filePath);
		test.displayData();
	}

	@Test
	public void testMain() throws Exception {
		ExcelUploader.main(null);
	}

	@Test(expected = Exception.class)
	public void testInsertdata() throws Exception {
		String filePath = "ddd";
		ExcelUploader test = new ExcelUploader(filePath);
		int rowOffset = 7;
		test.setRowOffset(rowOffset);
		test.insertData();
	}
 
	@Test(expected = InvalidFormatException.class)
	public void testInvalidFormat() throws Exception {
		String filePath = "ddd.pdf";
		ExcelUploader test = new ExcelUploader(filePath);
		test.checkfileFormat();
	}

	@Test(expected = java.io.FileNotFoundException.class)
	public void testfileNotFound() throws Exception {
		String filePath = "ddd";
		ExcelUploader test = new ExcelUploader(filePath);
		int rowOffset = 7;
		test.setRowOffset(rowOffset);
		test.insertData();
	}

	/*
	 * @Test(expected = NullPointerException.class) public void
	 * testnullPointer()throws Exception { String filePath=""; ExcelUploader
	 * test = new ExcelUploader(filePath); int rowOffset=7;
	 * test.setRowOffset(rowOffset); test.insertData(); }
	 */

}
