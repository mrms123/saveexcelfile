package acgPortal.saveExcelFile;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {

	public static Properties loadPropertiesFile() throws Exception {

		Properties prop = new Properties();
		InputStream in = new FileInputStream("src/resources/jdbc.properties");
		prop.load(in);
		in.close();
		return prop;
	}

	public static Connection getConnection() {
		Connection con = null;

		try {

			Properties prop = loadPropertiesFile();

			String driverClass = prop.getProperty("JDBC.driver");
			String url = prop.getProperty("JDBC.url");
			String username = prop.getProperty("JDBC.username");
			String password = prop.getProperty("JDBC.password");

			Class.forName(driverClass);

			con = DriverManager.getConnection(url, username, password);

			if (con == null) {
				System.out.println("unable to create connection");
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;

	}

}
