package acgPortal.saveExcelFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUploader {

	private int rowOffset;
	String filePath;

	ExcelUploader(String filePath) {
		this.filePath = filePath;
	}

	public boolean isValidPath() {
		try {
			Paths.get(filePath);
		} catch (InvalidPathException ex) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}

	public boolean checkfileFormat() {

		String filename = new File(filePath).getName();
		String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
		String excel = "xlsx";
		if (!extension.equals(excel)) {
			return false;
		} else {
			return true;
		}

	}

	public void insertData() throws SQLException, IOException {
	/*	try {*/

			Connection connectionString = DatabaseConnection.getConnection();
			PreparedStatement sqlStatement = null;
			String insertStatement = "INSERT INTO citi_timesheet"
					+ "(date_field, team_member, project, module, phase, activity, description, ticket_id, total_hours, time_spent) VALUES"
					+ "(?,?,?,?,?,?,?,?,?,?)";

			sqlStatement = connectionString.prepareStatement(insertStatement);
			FileInputStream fileInput = new FileInputStream(new File(filePath));
			XSSFWorkbook workBook = new XSSFWorkbook(fileInput);
			XSSFSheet sheet = workBook.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() < rowOffset) {
					continue;
				}
				Iterator<Cell> cellIterator = row.cellIterator();
				int columnIndex = 1;
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getCellTypeEnum()) {
					case STRING:
						sqlStatement.setString(columnIndex++, cell.getStringCellValue());
						break;
					case NUMERIC:
						if (DateUtil.isCellDateFormatted(cell)) {
							if (cell.getColumnIndex() == 9) {
								sqlStatement.setTime(columnIndex++,
										new java.sql.Time(cell.getDateCellValue().getTime()));
							} else {
								sqlStatement.setDate(columnIndex++,
										new java.sql.Date(cell.getDateCellValue().getTime()));
							}
						} else {
							sqlStatement.setDouble(columnIndex++, cell.getNumericCellValue());
						}
						break;

					default:
						columnIndex++;
					}

				}

				sqlStatement.addBatch();
			}
			sqlStatement.executeBatch();
			connectionString.commit();

		} 

	public void displayData() throws Exception {
		try

		{
			Connection connectionString = DatabaseConnection.getConnection();
			String selectStatement = "SELECT * FROM citi_timesheet";
			Statement statement = connectionString.createStatement();
			ResultSet resultset = statement.executeQuery(selectStatement);
			if (!resultset.next()) {
				System.out.println("No data");
			} else {
				while (resultset.next()) {

					ResultSetMetaData dbData = resultset.getMetaData();

					int columnCount = dbData.getColumnCount();

					for (int iterator = 1; iterator <= columnCount; iterator++) {
						System.out.println(dbData.getColumnName(iterator) + " : " + resultset.getString(iterator));
					}
				}
			}

			connectionString.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void main(String[] args) throws Exception {
		try {
			Scanner input = new Scanner(System.in);

			System.out.println("Enter the File Path : ");
			String filesPath = input.nextLine();

			System.out.println("Enter the number of rows you want to skip : ");
			Integer rowOffset = input.nextInt();
			ExcelUploader obj = new ExcelUploader(filesPath);

			boolean isValid = obj.isValidPath();

			if (isValid) {

				boolean isValidFormat = obj.checkfileFormat();
				if (isValidFormat) {
					obj.setRowOffset(rowOffset);
					obj.insertData();
					obj.displayData();
				} else {
					System.out.println("Invalid Format, Enter .xlsx file only");
				}

			} else {
				System.out.println("Invalid Path");
			}

			input.close();

		} catch (InvalidFormatException e) {
			System.err.println("ERROR : Please upload .xlsx files only !");
		}

		catch (InputMismatchException e) {
			System.err.println("ERROR : Invalid input !");
		}

		catch (FileNotFoundException e) {
			System.err.println("ERROR : File not found !");
		}

		catch (IOException e) {

			System.err.println("ERROR : File Opening Failed!");

		}

		catch (NullPointerException e) {
			System.err.println("ERROR : A Null Pointer Exception was caught!");
		}

		catch (Exception e) {
			System.out.println(e);
		}

	}

	public int getRowOffset() {
		return rowOffset;
	}

	public void setRowOffset(int rowOffset) {
		this.rowOffset = rowOffset;
	}

}
